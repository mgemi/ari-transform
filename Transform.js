/*
 * @author Ari Zerner
 */

/**
 * Apply a sequence of transformations to a value. Transformations are obtained
 * by calling the helper functions of transform. Anywhere a transformation is
 * required (i.e. here or in a higher-order transformation), null or undefined
 * may be used as shortcuts for transform.identity, a string may be used as a
 * shortcut for transform.prop, and an array of transformations a may be used as
 * a shortcut for transform.compose.
 *
 * @param value
 *          the value to transform
 * @param transformations
 *          (rest arg) transformations to apply sequentially
 * @returns the result of applying all of the transformations
 */
function transform(value, transformations) {
  return transform.compose([].slice.call(arguments, 1))(value);
}

module.exports = transform;

// transformations

/**
 * Leave the value unchanged.
 */
transform.identity = () => value => value;

/**
 * Apply a function to the value.
 *
 * @param f
 *          the function to apply
 */
transform.apply = f => value => f(value);

/**
 * Ignores the input value and outputs a constant.
 *
 * @param constant
 *          the constant to output
 */
transform.constant = constant => value => constant;

/**
 * Replace the value if it is null or undefined, otherwise leave the value
 * unchanged.
 *
 * @param replacement
 *          the new value
 */
transform.orElse = replacement => value => exists(value) ? value : replacement;

/**
 * Get a property of the value if the value is not null or undefined, otherwise
 * leave the value unchanged.
 *
 * @param property
 *          the property to get
 */
transform.prop = property => value => exists(value) ? value[property] : value;

/**
 * Look up the value in an object. Similar to transform.prop, but inverted, such
 * that the value is the property and the object is provided to the
 * transformation.
 *
 * @param obj
 *          the object to look up the value in
 */
transform.lookUp = obj => value => obj[value];

/**
 * Wrap the value in a promise.
 */
transform.promise = () => value => Promise.resolve(value);

/**
 * Wrap the value in a generator, a no-argument function that returns the value.
 */
transform.generator = () => value => () => value;

// higher-order transformations

/**
 * Compose a sequence of transformations into a single transformation.
 *
 * @param transformations
 *          the transformations to compose
 */
transform.compose = transformations => {
  return transformations.map(applyShortcut).reduce(
    (acc, next) => value => next(acc(value)),
    transform.identity()
  );
  /*
   * We call transformations directly here, because transform calls
   * transform.compose. Everywhere else, transformations should be called via
   * transform for refactorability.
   */
}

/**
 * Build an object or array out of transformations of the value.
 *
 * @param spec
 *          an object or array wherein every value is a transformation
 */
transform.build = spec => value => {
  var build = Array.isArray(spec) ? [] : {};
  for (var key in spec) {
    if (spec.hasOwnProperty(key)) {
      build[key] = transform(value, spec[key]);
    }
  }
  return build;
};

/**
 * Choose a transformation to apply to the value based on the result of a
 * predicate transformation.
 *
 * @param predicateTransformation
 *          the transformation to apply to the value in order to obtain the test
 *          result
 * @param trueTransformation
 *          the transformation to apply to the value if the test result is
 *          truthy
 * @param falseTransformation
 *          the transformation to apply to the value if the test result is
 *          falsey
 */
transform.if =
  (predicateTransformation, trueTransformation, falseTransformation) => value => {
    if (transform(value, predicateTransformation)) {
      return transform(value, trueTransformation);
    } else {
      return transform(value, falseTransformation);
    }
  };

// object transformations
transform.object = {};

/**
 * Transform a property of the value. The value must be an object (or array).
 * This transformation DOES NOT mutate the original object. If the key is not in
 * the object, the property is taken to have a value of undefined.
 *
 * @param key
 *          the key of the property
 * @param transformation
 *          the transformation to apply to the property
 */
transform.object.update = (key, transformation) => value => {
  var copy = Object.assign(Array.isArray(value) ? [] : {}, value);
  copy[key] = transform(value[key], transformation);
  return copy;
};

// array transformations
transform.array = {};

/**
 * Map a transformation over the value, transforming each element. The value
 * must be an array.
 *
 * @param transformation
 *          the transformation to apply to each element of the value
 */
transform.array.map = transformation => value => {
  return value.map(element => transform(element, transformation));
};

/**
 * Filter the value using a predicate transformation, removing all elements that
 * turn falsey when transformed with the predicate transformation. The value
 * must be an array.
 *
 * @param predicateTransformation
 *          the predicate transformation
 */
transform.array.filter = predicateTransformation => value => {
  return value.filter(element => transform(element, predicateTransformation));
};

/**
 * Reduce the value by converting each element into a transformation, then
 * transforming a given initial value with the resulting sequence of
 * transformations. The value must be an array.
 *
 * @param converter
 *          a function that takes an element of the value and returns a
 *          transformation
 * @param initialValue
 *          the initial value to transform
 */
transform.array.reduce = (converter, initialValue) => value => {
  return transform(initialValue, value.map(converter));
};

// utility functions

function exists(value) {
  return value != null && typeof value != 'undefined';
}

function applyShortcut(transformation) {
  if (!exists(transformation)) {
    return transform.identity();
  } else if (typeof transformation in {
      'string': true,
      'number': true
    }) {
    return transform.prop(transformation);
  } else if (Array.isArray(transformation)) {
    return transform.compose(transformation);
  } else {
    return transformation;
  }
}
