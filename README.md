# transform-value

> Pass a value through a chain of transformations.

## Install
```
$ npm install --save transform-value
```

## Usage
```js
const transform = require('transform-value')

transform({foo: 3}, 'foo', transform.orElse(0), transform.apply(n => n + n))
//=> 6
transform(null, 'foo', transform.orElse(0), transform.apply(n => n + n))
//=> 0
transform([null, 1, 2, 3], transform.build({
  length: 'length',
  original: transform.identity(),
  squares: transform.array.map([
    transform.orElse(0),
    transform.apply(n => n * n)
  ]),
  constant: transform.constant(42)
}))
/* =>
{
  length: 4,
  original: [null, 1, 2, 3],
  squares: [0, 1, 4, 9],
  constant: 42
}
*/
```
