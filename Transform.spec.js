/*
 * @author Ari Zerner
 */
'use strict';

const should = require('chai').should();

const transform = require('./Transform.js');

describe('transform', () => {
  function describeT(name, tests) {
    if (!Array.isArray(tests)) tests = [].slice.call(arguments, 1);
    describe(name, () => tests.forEach(itT));
  }

  function itT({
    itShould,
    // either this
    customTest,
    // or these
    value,
    transformation,
    expectedChange
  }) {
    it(`should ${itShould}`, () => {
      if (customTest) {
        customTest()
      } else {
        const expected = expectedChange ? expectedChange(value) : value;
        const actual = transform(value, transformation);
        if (expected == null) {
          (actual == null).should.be.true;
        } else if (typeof expected == 'undefined') {
          (typeof actual == 'undefined').should.be.true;
        } else {
          actual.should.deep.equal(expected);
        }
      }
    });
  }

  function id(v) {
    return v;
  }

  itT({
    itShould: 'leave a value unchanged when given no transformations',
    customTest: () => transform('foo').should.equal('foo')
  })

  itT({
    itShould: 'leave a value unchanged when given an empty array of transformations',
    value: 'foo',
    transformation: [],
    expectedChange: id
  });

  describe('shortcuts', () => {
    describeT('a string', {
      itShould: 'be treated as transform.prop',
      value: {
        foo: 'bar'
      },
      transformation: 'foo',
      expectedChange: v => transform(v, transform.prop('foo'))
    });

    describeT('a number', {
      itShould: 'be treated as transform.prop',
      value: ['foo', 'bar'],
      transformation: 0,
      expectedChange: v => transform(v, transform.prop(0))
    });

    describeT('null', {
      itShould: 'be treated as transform.identity',
      value: 'foo',
      transformation: null,
      expectedChange: v => transform(v, transform.identity())
    });

    describeT('undefined', {
      itShould: 'be treated as transform.identity',
      value: 'foo',
      transformation: undefined,
      expectedChange: v => transform(v, transform.identity())
    });

    describeT('an array', {
      itShould: 'be treated as transform.compose',
      value: null,
      transformation: [
        transform.orElse({
          foo: 'bar'
        }),
        transform.prop('foo')
      ],
      expectedChange: v => transform(v, transform.compose([
        transform.orElse({
          foo: 'bar'
        }),
        transform.prop('foo')
      ]))
    });
  });

  /*
   * Several of the following tests test every kind of shortcut. I'm leaving
   * that in place because more coverage doesn't hurt, but going forward, only
   * one kind of shortcut needs to be tested for any given transformation,
   * unless there's a specific reason to think a transformation might treat
   * shortcuts differently.
   */

  describeT('transform.identity', {
    itShould: 'leave a value unchanged',
    value: 'foo',
    transformation: transform.identity(),
    expectedChange: id
  });

  describeT('transform.apply', {
    itShould: 'apply a function to a value',
    value: 'foo',
    transformation: transform.apply(v => v.length),
    expectedChange: v => v.length
  });

  describeT('transform.constant', {
    itShould: 'transform a value into a constant',
    value: 'foo',
    transformation: transform.constant('bar'),
    expectedChange: v => 'bar'
  })

  describeT('transform.orElse', [{
      itShould: 'replace null',
      value: null,
      transformation: transform.orElse('foo'),
      expectedChange: v => 'foo'
    },
    {
      itShould: 'replace undefined',
      value: undefined,
      transformation: transform.orElse('foo'),
      expectedChange: v => 'foo'
    }
  ].concat(['bar', '', 0, false].map(v => {
    return {
      itShould: `not replace ${typeof v == 'string' ? `'${v}'` : v}`,
      value: v,
      transformation: transform.orElse('foo'),
      expectedChange: id
    };
  })));

  describeT('transform.prop', {
    itShould: 'get a property of an object',
    value: {
      foo: 'bar'
    },
    transformation: transform.prop('foo'),
    expectedChange: v => v.foo
  }, {
    itShould: 'get a property of an array',
    value: ['foo', 'bar'],
    transformation: transform.prop(1),
    expectedChange: v => v[1]
  }, {
    itShould: 'not get a property of null',
    value: null,
    transformation: transform.prop('foo'),
    expectedChange: id
  }, {
    itShould: 'not get a property of undefined',
    value: undefined,
    transformation: transform.prop('foo'),
    expectedChange: id
  });

  describeT('transform.lookUp', {
    itShould: 'look up a property of an object',
    value: 'foo',
    transformation: transform.lookUp({
      foo: 'bar'
    }),
    expectedChange: v => ({foo: 'bar'})[v]
  }, {
    itShould: 'look up an element of an array',
    value: 1,
    transformation: transform.lookUp(['foo', 'bar']),
    expectedChange: v => ['foo', 'bar'][1]
  })

  describeT('transform.promise', {
    itShould: 'wrap a value in a promise',
    customTest: () => {
      const value = 'foo';
      transform(value, transform.promise()).then(res => {
        res.should.equal(value);
      });
    }
  });

  describeT('transform.generator', {
    itShould: 'wrap a value in a generator',
    customTest: () => {
      const value = 'foo';
      transform(value, transform.generator())().should.equal(value);
    }
  });

  describeT('transform.compose', {
    itShould: 'compose no transformations into the equivalent of transform.identity',
    value: 'foo',
    transformation: transform.compose([]),
    expectedChange: id
  }, {
    itShould: 'compose one transformation into the equivalent of that transformation',
    value: 'foo',
    transformation: transform.compose([transform.apply(v => v + 'bar')]),
    expectedChange: v => v + 'bar'
  }, {
    itShould: 'compose a sequence of transformations',
    value: null,
    transformation: transform.compose([
      transform.orElse('foo'),
      transform.apply(v => v + 'bar'),
      transform.prop('length')
    ]),
    expectedChange: v => ('foo' + 'bar').length
  }, {
    itShould: 'handle shortcuts',
    value: null,
    transformation: transform.compose([
      null, [transform.orElse('foo'), transform.apply(v => [v, v + 'bar'])],
      1, undefined, 'length'
    ]),
    expectedChange: v => ('foo' + 'bar').length
  });

  describeT('transform.build', {
    itShould: 'build an array',
    customTest: () => {
      const build = transform.build([
        transform.prop('length'),
        transform.identity(),
        transform.compose([transform.constant(42), transform.apply(v => v + v)])
      ]);
      transform('foo', build).should.deep.equal([
        3,
        'foo',
        84
      ]);
      transform('barbar', build).should.deep.equal([
        6,
        'barbar',
        84
      ]);
    },
  }, {
    itShould: 'build an object',
    customTest: () => {
      const build = transform.build({
        length: transform.prop('length'),
        id: transform.identity(),
        c: transform.compose([transform.constant(42), transform.apply(v => v + v)])
      });
      transform('foo', build).should.deep.equal({
        length: 3,
        id: 'foo',
        c: 84
      });
      transform('barbar', build).should.deep.equal({
        length: 6,
        id: 'barbar',
        c: 84
      });
    }
  }, {
    itShould: 'handle shortcuts when building an array',
    value: 'foo',
    transformation: transform.build([
      'length',
      null,
      undefined,
      [transform.constant(42), transform.apply(v => v + v)],
      0
    ]),
    expectedChange: v => [v.length, v, v, 42 + 42, v[0]]
  }, {
    itShould: 'handle shortcuts when building an object',
    value: 'foo',
    transformation: transform.build({
      length: 'length',
      n: null,
      u: undefined,
      c: [transform.constant(42), transform.apply(v => v + v)],
      first: 0
    }),
    expectedChange: v => {
      return {
        length: v.length,
        n: v,
        u: v,
        c: 42 + 42,
        first: 'f'
      }
    }
  });

  describeT('transform.if', {
    itShould: 'handle the true case',
    value: 4,
    transformation: transform.if(
      transform.apply(v => v % 2 == 0),
      transform.apply(v => v / 2),
      transform.apply(v => 3 * v + 1)
    ),
    expectedChange: v => v / 2
  }, {
    itShould: 'handle the false case',
    value: 5,
    transformation: transform.if(
      transform.apply(v => v % 2 == 0),
      transform.apply(v => v / 2),
      transform.apply(v => 3 * v + 1)
    ),
    expectedChange: v => 3 * v + 1
  }, {
    itShould: 'handle null & string shortcuts',
    value: [1, 2, 3],
    transformation: transform.if(null, 'length', null),
    expectedChange: v => v.length
  }, {
    itShould: 'handle undefined & number shortcuts',
    value: [1, 2, 3],
    transformation: transform.if(undefined, 1, null),
    expectedChange: v => v[1]
  }, {
    itShould: 'handle array shortcut',
    value: [1, 2, 3],
    transformation: transform.if(['length', 'length'], null, [null, 'length']),
    expectedChange: v => v.length
  });

  describeT('transform.object.update', {
    itShould: 'not modify the original object',
    customTest: () => {
      const obj = {
        foo: 'foo',
        bar: 'bar'
      };
      transform(obj, transform.object.update('bar', transform.constant('baz')))
      obj.should.deep.equal({
        foo: 'foo',
        bar: 'bar'
      });
    }
  }, {
    itShould: 'update an object',
    value: {
      foo: 'foo',
      bar: 'bar'
    },
    transformation: transform.object.update('bar', transform.constant('baz')),
    expectedChange: v => Object.assign({}, v, {
      bar: 'baz'
    })
  }, {
    itShould: 'treat a missing property as having a value of undefined',
    value: {
      foo: 'foo'
    },
    transformation: transform.object.update('bar', transform.orElse('baz')),
    expectedChange: v => Object.assign({}, v, {
      bar: 'baz'
    })
  }, {
    itShould: 'work on arrays',
    value: [1, 2, 3],
    transformation: transform.object.update(2, transform.apply(v => v + 1)),
    expectedChange: v => Object.assign([], v, {
      2: 4
    })
  }, {
    itShould: 'handle shortcuts',
    value: {
      foo: 'foo',
      bar: {
        baz: 'baz'
      }
    },
    transformation: transform.object.update('bar', 'baz'),
    expectedChange: v => Object.assign({}, v, {
      bar: 'baz'
    })
  });

  describeT('transform.array.map', [{
    itShould: 'transform each element of an array',
    value: [1, null, 3],
    mappedTransformation: transform.orElse(2),
    expectedMappedChange: e => e ? e : 2
  }, {
    itShould: 'handle null shortcut',
    value: [1, 2, 3],
    mappedTransformation: null,
    expectedMappedChange: id
  }, {
    itShould: 'handle undefined shortcut',
    value: [1, 2, 3],
    mappedTransformation: undefined,
    expectedMappedChange: id
  }, {
    itShould: 'handle string shortcut',
    value: ['1', '22', '333'],
    mappedTransformation: 'length',
    expectedMappedChange: e => e.length
  }, {
    itShould: 'handle number shortcut',
    value: ['312', '123', '231'],
    mappedTransformation: 1,
    expectedMappedChange: e => e[1]
  }, {
    itShould: 'handle array shortcut',
    value: [null, 2, 3],
    mappedTransformation: [transform.orElse(1), transform.apply(e => e * 2)],
    expectedMappedChange: e => e ? e * 2 : 2
  }].map(({
    itShould,
    value,
    mappedTransformation,
    expectedMappedChange
  }) => {
    return {
      itShould: itShould,
      value: value,
      transformation: transform.array.map(mappedTransformation),
      expectedChange: v => v.map(expectedMappedChange)
    };
  }));

  describeT('transform.array.filter', [{
    itShould: 'transform each element of an array',
    value: [1, 2, 3],
    predicateTransformation: transform.apply(e => e % 2 != 0),
    expectedPredicate: e => e % 2 != 0
  }, {
    itShould: 'handle null shortcut',
    value: [1, null, 3, false],
    predicateTransformation: null,
    expectedPredicate: id
  }, {
    itShould: 'handle undefined shortcut',
    value: [1, null, 3, false],
    predicateTransformation: undefined,
    expectedPredicate: id
  }, {
    itShould: 'handle string shortcut',
    value: ['1', 2, [3]],
    predicateTransformation: 'length',
    expectedPredicate: v => v.length
  }, {
    itShould: 'handle number shortcut',
    value: ['1', 2, [3]],
    predicateTransformation: 0,
    expectedPredicate: v => v[0]
  }, {
    itShould: 'handle array shortcut',
    value: [null, 2, 3],
    predicateTransformation: [
      transform.orElse(1),
      transform.apply(v => v % 2 != 0)
    ],
    expectedPredicate: v => (v ? v : 1) % 2 != 0
  }].map(({
    itShould,
    value,
    predicateTransformation,
    expectedPredicate
  }) => {
    return {
      itShould: itShould,
      value: value,
      transformation: transform.array.filter(predicateTransformation),
      expectedChange: v => v.filter(expectedPredicate)
    };
  }));

  describeT('transform.array.reduce', [{
    itShould: 'reduce an array',
    value: [1, 2, 3],
    accumulator: e => transform.apply(v => e + v),
    initial: 0,
    expectedReduction: (a, b) => a + b
  }, {
    itShould: 'handle shortcuts',
    value: [null, 'foo', 0, undefined, ['bar', 2]],
    accumulator: id,
    initial: {
      foo: [{
        bar: 'baz'
      }]
    },
    expectedReduction: (a, b) => 'z' // just trust me on this one
  }].map(({
    itShould,
    value,
    accumulator,
    initial,
    expectedReduction
  }) => {
    return {
      itShould: itShould,
      value: value,
      transformation: transform.array.reduce(accumulator, initial),
      expectedChange: v => v.reduce(expectedReduction, initial)
    };
  }));
});
